

function each(arr,callback){
// 'each' traverse every array item one by one
    if (arr && callback && (typeof callback === 'function') && Array.isArray(arr)){
        for(let i=0; i<arr.length; i++){
            callback(arr[i],i,arr);
        }
    }
}


function map(arr,callback){
// 'map' return a new manipulated array
    let result = [];
    if (arr && callback && (typeof callback === 'function') && Array.isArray(arr)){
        for(let i=0; i<arr.length; i++){
            let temp = callback(arr[i],i,arr);
            if (typeof temp !== 'undefined'){
                result.push(temp);
            }
        }
    }
    return result;
}



function reduce(arr,callback,cValue){
// 'reduce' return a single value
    if (arr && callback && (typeof callback === 'function') && Array.isArray(arr)){
        if (typeof cValue === 'undefined'){
            cValue = arr[0];
        }
        for(let i=0; i<arr.length; i++){
            cValue = callback(cValue,arr[i],i,arr);
        }
    }
    return cValue
}



function find(arr,callback){
// 'find' return first occurrence of value
    let fValue;
    if (arr && callback && (typeof callback === 'function') && Array.isArray(arr)) {
        for(let i=0; i<arr.length; i++) {
            if (callback(arr[i], i, arr)) {
                fValue = arr[i]; break;
            }
        }
    }
    return fValue;
}


function filter(arr,callback){
// 'filter' return a new array that satisfied user condition
    let result = []
    if (arr && callback && (typeof callback === 'function') && Array.isArray(arr)){
        for(let i=0; i<arr.length; i++){
            if (callback(arr[i],i,arr)){
                result.push(arr[i]);
            }
        }
    }
    return result;
}


function makeFlat(i){
// helper function for flatten 
    if (typeof i === 'number'){
        return i;
    } else {
       return makeFlat(i[0]);
    }
}

function flatten(arr){
// return new array having trimmed extra []
    let result = [];
    for(let i=0; i<arr.length; i++){
        let temp = makeFlat(arr[i]);
        if(typeof temp !== 'undefined'){
            result.push(temp);
        }
    }
    return result;
}


module.exports = {each,map,reduce,find,filter,flatten};