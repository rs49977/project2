
function counterFactory(){
    // it return obj having method increment and decrement
    let count = 10;
    const obj = {
        increment () { return ++count; },
        decrement () { return --count; }
    }
    return obj;
}


function limitFunctionCallCount(func,num){
    // it limit the function 'func' call 'num' times
    let count = num;
    function limitCall () {
        if(count && typeof func == 'function'){
            --count;
            return func;
        } else {
            return null;
        }
    }
    return limitCall();
}

function cacheFunction(func) {
    // return value form 'obj' if present else call 'func'
    let obj = {};
    function helper(arg){
        if (arg in obj){
            return obj[arg];
        } else {
            obj[arg] = func(arg);
            return obj[arg];
        }
    }  
  return helper;
}



module.exports = {cacheFunction,limitFunctionCallCount,counterFactory}