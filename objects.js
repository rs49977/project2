

// 'keys' returns collection of key
function keys(obj){
    let objKeys = [];
    if(typeof obj === 'object' && obj != null){
        for (let key in obj){
            objKeys.push(key);
        }
    }
    return objKeys;
}

// 'values' returns collection of value
function values(obj){
    let objValues = [];
    if(typeof obj === 'object' && obj != null){
        for (let key in obj){
            objValues.push(obj[key]);
        }
    }
    return objValues;
}

// 'mapObject' return new object, manipulated values
function mapObject(obj, callback) {
    let newObj = {};
    if (typeof obj === 'object' && obj != null && callback && (typeof callback === 'function')){
        for(let key in obj){
            newObj[key] = callback(obj[key],key,obj);
        }
    }
    return newObj;
}

// 'pairs' returns array of key,value array 
function pairs(obj){
    let arrPairs = [];
    if(typeof obj === 'object' && obj != null){
        for (let key in obj){
            arrPairs.push([key,obj[key]]);
        }
    }
    return arrPairs;
}

// 'invert' returns new object having swapped key,value
function invert(obj){
    let invObj = {};
    if(typeof obj === 'object' && obj != null){
        for (let key in obj){
            invObj[obj[key]] = key;
        }
    }
    return invObj;
}

// 'keys' returns collection of key
function defaults(obj,dPros) {
    if(typeof obj === 'object' && obj != null && typeof dPros === 'object' && dPros != null){
        // using above implemented keys function
        let key = keys(dPros)[0];
        obj[key] = dPros[key];
    }
    return obj;
}

module.exports = {keys,values,mapObject,pairs,invert,defaults};